'use strict';
const https = require('https');
const fs = require('fs');
const WebSocket = require('ws');
var os = require('os');
var pty = require('node-pty');
var path = require('path');
var keyPath = path.join(__dirname, 'key.pem');
var certPath = path.join(__dirname, 'cert.pem');

var privateKey = fs.readFileSync (keyPath, 'utf8');
var certificate = fs.readFileSync( certPath, 'utf8');


var credentials = {
  key: privateKey,
  cert: certificate
};
var express = require('express');
var app = express();

//pass in your express app and credentials to create an https server
var httpsServer = https.createServer(credentials, app);
httpsServer.listen(3001, function() {
  console.log("listening on 3001");
});
var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({
  server: httpsServer
});

wss.on('connection', function connection(ws) {
  var shell = os.platform() === 'win32' ? 'powershell.exe' : 'bash';
  var ptyProcess = pty.spawn(shell, [], {
    name: 'xterm-color',
    cols: 80,
    rows: 30,
    cwd: process.env.HOME,
    env: process.env
  });
  ptyProcess.on('data', function(data) {
    ws.send(data);
  });
  ws.on('message', function incoming(message) {
    ptyProcess.write(message);
  });

});

wss.on('close', function close() {
  ws.send("disconnected");
});
