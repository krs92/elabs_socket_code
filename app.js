"use strict";
var ws = require("nodejs-websocket");
var os = require('os');
var pty = require('node-pty');
var server = ws.createServer(function(conn) {
    // conn.on('connection', function() {
    //     console.log("on connect");
    // });

    var shell = os.platform() === 'win32' ? 'powershell.exe' : 'bash';
    var ptyProcess = pty.spawn(shell, [], {
        name: 'xterm-color',
        cols: 80,
        rows: 30,
        cwd: process.env.HOME,
        env: process.env
    });
    //conn.sendText(data)

    ptyProcess.on('data', function(data) {



      //  console.log("input ==="+output);

        //console.log("output"+data);
        conn.sendText(data)
    });
    conn.on("text", function(str) {
        //console.log("Received " + str + "con" + conn.path);
        ptyProcess.write(str);



    })
    conn.on("close", function(code, reason) {
        //console.log("Connection closed")
    })
}).listen(3001, function() {
   //console.log("soket server running on 3001")
})
